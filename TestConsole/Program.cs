﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GlobExpressions;
using Kirinnee.Helper;
using Kirinnee.ImageOrchestrator;
using Kirinnee.ImageOrchestrator.Transformers;

namespace TestConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Async().GetAwaiter().GetResult();
        }

        private static async Task Async()
        {
            Console.WriteLine("Beginning Compression");
            var sw = new Stopwatch();
            sw.Start();

            var files = Glob.Files("assets", "**/*.*").ToArray();
            Console.WriteLine(files.JoinBy("\n"));
            ImageTransformer pngquant = new PngQuantTransformer();
            ImageTransformer jpegoptim = new JpegOptimTransformer();
            ImageTransformer resizer = new PercentageResizeTransformer(0.75, new[] {"image/png", "image/jpeg"});
            var images = files
                .Select(s => new VirtualImage("assets", "compressed", s)).Select(s => s.Read());

            foreach (var image in images)
            {
                Console.WriteLine("Starting:" + image.RawFile);
                var x = image | resizer | pngquant | jpegoptim;
                Console.WriteLine("Done:" + image.RawFile);
                x.Write();
                Console.WriteLine("Written:" + image.RawFile);
            }


            Console.WriteLine("Compression took: " + sw.ElapsedMilliseconds + "ms for " + files.Length + " files.");
            Console.ReadLine();
            await Async();
        }
    }

    internal static class Utility
    {
        public static string Com(this string directory, string path)
        {
            return Path.Combine(directory, path);
        }
    }
}