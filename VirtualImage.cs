using System;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using Kirinnee.Helper;
using Kirinnee.ImageOrchestrator.Transformers;

namespace Kirinnee.ImageOrchestrator
{
    public class VirtualImage
    {
        /// <summary>
        /// The context folder to read from
        /// </summary>
        public string FromContext { get; set; }

        /// <summary>
        /// The context folder to write to
        /// </summary>
        public string ToContext { get; set; }

        /// <summary>
        /// The file name (include common directories and stuff)
        /// </summary>
        public string RawFile { get; private set; }

        /// <summary>
        /// The bytes of the blob
        /// </summary>
        public byte[] Content { get; private set; }

        /// <summary>
        /// Gets the Full From path to read from
        /// </summary>
        public string FullFrom => Path.Combine(FromContext, RawFile);

        /// <summary>
        /// Gets the Full To path to write to
        /// </summary>
        public string FullTo => Path.Combine(ToContext, RawFile);

        private string _ext => RawFile.SplitBy(".").Last(1).ToArray()[0];


        /// <summary>
        /// Return the mime type of the bytes
        /// (For example "image/png" or "text/plain"
        /// </summary>
        public string MimeType => Content.GetMimeString();

        public VirtualImage(string from, string to, string image)
        {
            FromContext = from ?? throw new ArgumentNullException($"Cannot be null {nameof(from)}");
            ToContext = to ?? throw new ArgumentNullException($"Cannot be null {nameof(to)}");
            RawFile = image ?? throw new ArgumentNullException($"Cannot be null {nameof(image)}");
            Content = new byte[] { };
        }

        /// <summary>
        /// Read the image. 
        /// </summary>
        /// <returns></returns>
        public VirtualImage Read()
        {
            Content = File.ReadAllBytes(FullFrom);
            return this;
        }

        /// <summary>
        /// Write file content to To path
        /// </summary>
        /// <returns></returns>
        public VirtualImage Write()
        {
            var f = new FileInfo(FullTo);
            f.Directory.Create();
            File.WriteAllBytes(FullTo, Content);
            return this;
        }

        /// <summary>
        /// Transform the image bytes using a transformer
        /// </summary>
        /// <param name="transformer"></param>
        /// <returns></returns>
        public VirtualImage Transform(ImageTransformer transformer)
        {
            Content = transformer.Transform(Content);
            return this;
        }

        public VirtualImage ChangeExtension(string ext)
        {
            RawFile = RawFile.SplitBy(".").Omit(1).JoinBy(".") + "." + ext;
            return this;
        }

        /// <summary>
        /// Renames the raw file base on the lambda.
        /// This excludes extensions, but includes 
        /// </summary>
        /// <param name="fx">the lambda to rename the file</param>
        /// <returns></returns>
        public VirtualImage Rename(Func<string, string> fx)
        {
            var first = RawFile.SplitBy(".").Omit(1).JoinBy(".");
            first = fx(first);
            RawFile = first + "." + _ext;
            return this;
        }

        /// <summary>
        /// Renames the files to the new name. This excludes extensions.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public VirtualImage Rename(string x)
        {
            RawFile = x + "." + _ext;
            return this;
        }


        public VirtualImage Clone()
        {
            var bytes = new byte[Content.Length];
            Content.CopyTo(bytes, 0);
            return new VirtualImage(FromContext, ToContext, RawFile) {Content = bytes};
        }
    }
}