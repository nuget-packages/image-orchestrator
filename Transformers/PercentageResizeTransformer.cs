using System.IO;
using System.Linq;
using MimeDetective;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace Kirinnee.ImageOrchestrator.Transformers
{
    public class PercentageResizeTransformer : ImageTransformer
    {
        private readonly string[] _mimeTypes;
        private readonly double _percentage;

        public PercentageResizeTransformer(double percentage, string[] acceptedMimeType)
        {
            _percentage = percentage;
            _mimeTypes = acceptedMimeType;
        }

        public override byte[] Transform(byte[] input)
        {
            var mime = input.GetFileType().Mime;
            if (!_mimeTypes.Contains(mime)) return input;
            using (MemoryStream stream = new MemoryStream(input), outStream = new MemoryStream())
            {
                var image = Image.Load(stream, out var format);
                image.Mutate(x => x.Resize((int) (image.Width * _percentage), (int) (image.Height * _percentage)));
                image.Save(outStream, format);
                return outStream.ToArray();
            }
        }
    }
}